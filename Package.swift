// swift-tools-version:4.1
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "SwiftPreCommitHooks",
    products: [
      // Products define the executables and libraries produced by a package, and make them visible to other packages.
      // .library(name: "SwiftPreCommitHooks", targets: ["SwiftPreCommitHooks"]),
      .executable(name: "swiftlinter", targets: ["SwiftLinter"]),
      .executable(name: "test", targets: ["Test"]),
      .executable(name: "committer", targets: ["Committer"])
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
      .package(url: "https://github.com/JohnSundell/ShellOut.git", .upToNextMinor(from:  "2.1.0")),
      .package(url: "https://github.com/jakeheis/SwiftCLI.git", .upToNextMinor(from: "5.1.0"))
      
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages which this package depends on.
        // .target(
        //     name: "SwiftPreCommitHooks",
        //     dependencies: []),
        .target(
            name: "SwiftLinter",
            dependencies: ["ShellOut"]),
        .target(
            name: "Committer",
            dependencies: ["SwiftCLI", "ShellOut"]),
        .target(
            name: "Test",
            dependencies: []),
        // .testTarget(
        //     name: "SwiftPreCommitHooksTests",
        //     dependencies: ["SwiftPreCommitHooks"])
    ]
)
