import Foundation
import SwiftCLI
import ShellOut

extension String: Error { }

class NoCommit: Command {

  let name = "nocommit"
  
  let shortDescription = "Prevents a commit occuring on the current branch"

  let branches = VariadicKey<String>("--branch", "-b", description: "The branch that you aren't allowed to commit on")

  // let files = CollectedParameter()

  func execute() throws {

    do {
      // let dir = try shellOut(to: "echo $PWD", at: ".")
      
      // print("working dir = \(dir)")

      print("branches = \(branches.values)")

      // print("files = \(files.value)")

      // let indexes = files.value.indices.filter { files.value[$0] == "-b" || files.value[$0] == "--branch" }
      // let branches = indexes.map { files.value[$0 + 1] }
      // print("branches = \(branches)")
      // print(indexes)


      let action = "/usr/bin/git symbolic-ref HEAD"

      let output = try shellOut(to: action)

      print(output)

      let currentBranch = output.replacingOccurrences(of: "refs/heads/", with: "", options: .literal, range: nil)

      // if branches.values.contains(where: { $0 == currentBranch }) {

      //   print("🛑 Cannot commit on current branch: \(String(describing: currentBranch))")
         
      //   exit(1)

      // }

      print("💚 Commit allowed on branch: \(String(describing: currentBranch))")

      exit(0)
    } catch {
      print(error.localizedDescription)
      exit(1)
    }

  }

}
