import Foundation
import SwiftCLI

let version = "0.1.8"

let name = "nocommit"

let description = "Prevents a commit occuring on the current branch"

let commands: [Command] = [NoCommit()]

let worker = CLI(name: name, version: version, description: description, commands: commands)

let result = worker.go()

exit(result)

