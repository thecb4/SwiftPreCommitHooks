import Foundation
import ShellOut

func runSwiftLint() throws {
  print("running linter")
  do {
    // let output = try shellOut(to: "env")
    // print(output)
    try shellOut(to: "/usr/local/bin/swiftlint > swiftlint.report.json")
    print("results stored in ./swiftlint.report.json")
    exit(0)
  } catch {
    let error = error as! ShellOutError
    print(error.message) // Prints STDERR
    print(error.output) // Prints STDOUT
    exit(1)
  }
}
