import XCTest

import SwiftPreCommitHooksTests

var tests = [XCTestCaseEntry]()
tests += SwiftPreCommitHooksTests.allTests()
XCTMain(tests)
